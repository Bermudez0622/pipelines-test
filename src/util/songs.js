const songs = [
    {
        "name": "Guns N Roses (Use Your Illusion I)",
        "description": "Esta es la primera lista de reproducción",
        "songs": [
            {
                "title": "Sweet Child o' Mine",
                "artist": "Guns N Roses",
                "year": "1987"
            },
            {
                "title": "November Rain",
                "artist": "Guns N Roses",
                "year": "1990"
            },
            {
                "title": "Welcome to the Jungle",
                "artist": "Guns N Roses",
                "year": "1987"
            }
        ]
    },
    {
        "name": "PEACEFUL PIANO",
        "description": "Esta es la segunda lista de reproducción",
        "songs": []
    }
];

module.exports = songs;