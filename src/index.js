const express = require('express');
const { config } =  require('dotenv');
const songs  = require('./util/songs');

config({path:'.env'});

const app = express();

app.get('/api/v1/song-list',(req,res) => {
    res.status(200).send(songs);
});

app.listen( process.env.PORT || 4000, () => {
    console.log('Servidor Corriendo');
});